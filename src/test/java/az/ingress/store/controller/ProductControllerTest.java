package az.ingress.store.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.store.config.SecurityProperties;
import az.ingress.store.enums.dto.ProductDto;
import az.ingress.store.repository.ProductRepository;
import az.ingress.store.service.impl.ProductServiceImpl;
import java.math.BigDecimal;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ProductController.class)
@ContextConfiguration(classes = SecurityProperties.class)
@TestPropertySource
class ProductControllerTest {

    @Mock
    private ProductServiceImpl service;

    @Mock
    private ProductRepository productRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = "USER")
    public void whenGetAllProductsThenSuccess() throws Exception {
        //Arrange
        ProductDto productDto = new ProductDto();
        productDto.setPrice(BigDecimal.valueOf(200));
        productDto.setName("Alma");
        productDto.setCurrency("AZN");

        ProductDto productDto2 = new ProductDto();
        productDto2.setPrice(BigDecimal.valueOf(400));
        productDto2.setName("Armud");
        productDto2.setCurrency("USD");

        when(service.getAll()).thenReturn(Set.of(productDto, productDto2));

        //Act
        mockMvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(content().json(""));

    }
}