package az.ingress.store.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.store.enums.dto.ProductDto;
import az.ingress.store.mapper.ProductMapper;
import az.ingress.store.model.Product;
import az.ingress.store.repository.CategoryRepository;
import az.ingress.store.repository.ProductRepository;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductMapper mapper;
    @Mock
    private CategoryRepository categoryRepository;


    @Test
    public void givenIdWhenGetProductThenSuccess() {
        //Arrange
        Product product = new Product();
        product.setPrice(BigDecimal.valueOf(200));
        product.setName("Alma");
        product.setCurrency("AZN");

        ProductDto productDto = new ProductDto();
        productDto.setPrice(BigDecimal.valueOf(200));
        productDto.setName("Alma");
        productDto.setCurrency("AZN");

        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        when(mapper.mapEntityToDto(any())).thenReturn(productDto);

        //Akt
        ProductDto response = productService.getById(4L);

        //Assert
        assertThat(response.getName()).isEqualTo("Alma");
        assertThat(response.getCurrency()).isEqualTo("AZN");
        assertThat(response.getPrice()).isEqualTo(BigDecimal.valueOf(200));
        assertThat(response.getDescription()).isNull();

        verify(productRepository,times(1)).findById(anyLong());
        verify(mapper,times(1)).mapEntityToDto(any());
    }


    @Test
    public void givenIdWhenProductNotFoundThenException() {
        //Arrange
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        //Akt
        assertThatThrownBy(()-> productService.getById(1L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Product Id Not Found");
    }
}