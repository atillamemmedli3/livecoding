package az.ingress.store.enums;

public enum Currency {

    AZN(0);

    private final int value;

    Currency(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
