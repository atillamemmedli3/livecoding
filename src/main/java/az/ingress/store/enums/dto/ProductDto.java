package az.ingress.store.enums.dto;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {

    private Long id;
    private String name;
    private String ingredients;
    private String description;
    private BigDecimal price;
    private String currency;
    private Integer stockCount;
    private UUID imageId;

}
