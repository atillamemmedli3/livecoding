package az.ingress.store.enums.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasketProductDto {

    private Long id;
    private Long basketId;
    private Long productId;
    private Long productCount;
}
