package az.ingress.store.service;

import az.ingress.store.enums.dto.ProductDto;
import java.util.Set;

public interface ProductService {
    ProductDto create(ProductDto productDto);

    ProductDto getById(Long id);

    ProductDto updateById(Long id, ProductDto productDto);

    void deleteById(Long id);

    Set<ProductDto> getAll();

    ProductDto addCategoryToProduct(Long productId, Long categoryId);

    Page<Product> employeesPageable (Pageable pageable);

}
