package az.ingress.store.service;

import az.ingress.store.enums.dto.BasketProductDto;
import az.ingress.store.enums.Currency;
import az.ingress.store.model.Basket;
import az.ingress.store.model.BasketProduct;
import az.ingress.store.model.Product;
import az.ingress.store.repository.BasketRepository;
import az.ingress.store.repository.ProductRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final ProductRepository productRepository;
    private final BasketRepository basketRepository;

    @Override
    @Transactional
    public Boolean updateAndCreateBasket(BasketProductDto productDto) {
        log.info("update and create basket processing");
        Product product = productRepository.findById(productDto.getProductId())
                .orElseThrow(() ->
                        new RuntimeException(String.format("product not found with %s id", productDto.getProductId()))
                );
        basketRepository.findOneBasket().ifPresentOrElse(
                basket -> {
                    basket.setExpirationTime(LocalDateTime.now());
                    basket.setTotalAmount(calculateTotalAmount(productDto, product, Currency.AZN));
                    Set<BasketProduct> basketProducts = basket.getBasketProducts();
                    BasketProduct basketProduct = buildBasketProduct(productDto, product, basket);
                    basketProducts.add(basketProduct);
                    basket.setBasketProducts(basketProducts);
                    basketRepository.save(basket);
                },
                () -> {
                    var basket = buildBasket(productDto, product);
                    BasketProduct basketProduct = BasketProduct.builder()
                            .product(product)
                            .basket(basket)
                            .productCount(productDto.getProductCount())
                            .build();
                    basket.setBasketProducts(Set.of(basketProduct));
                    basketRepository.save(basket);
                });
        return true;
    }

    private BasketProduct buildBasketProduct(BasketProductDto productDto, Product product, Basket basket) {
        return BasketProduct.builder()
                .product(product)
                .basket(basket)
                .productCount(productDto.getProductCount())
                .build();
    }

    private Basket buildBasket(BasketProductDto productDto, Product product) {
        return Basket.builder()
                .totalAmount(calculateTotalAmount(productDto, product, Currency.AZN))
                .currency(Currency.AZN)
                .confirmed(false)
                .isActive(true)
                .expirationTime(null)
                .build();
    }

    private BigDecimal calculateTotalAmount(BasketProductDto productDto, Product product, Currency currency) {
        log.info("calculate total amount");
        BigDecimal totalCount = new BigDecimal(productDto.getProductCount());
        if (currency == Currency.AZN && product.getCurrency().equals("AZN")) {
            return totalCount.multiply(product.getPrice());
        } else {
            throw new IllegalArgumentException("currency not valid, currency is :" + currency);
        }

    }
}
