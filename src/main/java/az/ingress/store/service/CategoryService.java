package az.ingress.store.service;

import az.ingress.store.enums.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    CategoryDto create(CategoryDto categoryDto);
     List<CategoryDto> getAllCategory();
}

