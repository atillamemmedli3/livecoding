package az.ingress.store.service;

import az.ingress.store.enums.dto.BasketProductDto;

public interface BasketService {
    Boolean updateAndCreateBasket(BasketProductDto productDto);
}
