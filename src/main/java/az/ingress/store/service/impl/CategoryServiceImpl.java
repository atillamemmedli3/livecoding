package az.ingress.store.service.impl;

import az.ingress.store.enums.dto.CategoryDto;
import az.ingress.store.mapper.CategoryMapper;
import az.ingress.store.model.Category;
import az.ingress.store.repository.CategoryRepository;
import az.ingress.store.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryMapper mapper;

    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        return mapper.mapEntityToDto(categoryRepository.save(mapper.mapDtoToEntity(categoryDto)));
    }

    @Override
    public List<CategoryDto> getAllCategory() {
        List<Category> all = categoryRepository.findAll();
        List<CategoryDto> categoryList = all.stream().map(category -> mapper.mapEntityToDto(category)).collect(Collectors.toList());
        return categoryList;
    }
}
