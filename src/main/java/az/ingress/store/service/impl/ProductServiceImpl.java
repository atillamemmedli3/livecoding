package az.ingress.store.service.impl;

import az.ingress.store.enums.dto.ProductDto;
import az.ingress.store.mapper.ProductMapper;
import az.ingress.store.model.Category;
import az.ingress.store.model.Product;
import az.ingress.store.repository.CategoryRepository;
import az.ingress.store.repository.ProductRepository;
import az.ingress.store.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper mapper;
    private final CategoryRepository categoryRepository;

    @Override //Fidan , Elmir
    public ProductDto create(ProductDto productDto) {
        return mapper.mapEntityToDto(productRepository.save(mapper.mapDtoToEntity(productDto)));
    }

    @Override
    public ProductDto getById(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Product Id Not Found"));
        ProductDto productDto = mapper.mapEntityToDto(product);
        return productDto;
    }

    @Override //Azer, Orxan
    public ProductDto updateById(Long id, ProductDto productDto) {
        productRepository.findById(id)
                .ifPresentOrElse(entity -> productDto.setId(entity.getId()),
                        () -> new RuntimeException("Product Not Found"));
        return mapper.mapEntityToDto(productRepository.save(mapper.mapDtoToEntity(productDto)));
    }

    @Override //Elvin
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override //Javanshir, Taleh
    public Set<ProductDto> getAll() {
        return productRepository.findAll()
                .stream().map(entity -> (entity != null) ? mapper.mapEntityToDto(entity) : null)
                .collect(Collectors.toSet());
    }

    @Override //Atilla
    public ProductDto addCategoryToProduct(Long productId, Long categoryId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Not Found"));
        Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new RuntimeException("Not Found"));
        product.setCategories(Set.of(category));
        return mapper.mapEntityToDto(product);
    }

    @Override
    public Page<Product> employeesPageable(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}
