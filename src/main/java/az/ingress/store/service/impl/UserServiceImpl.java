package az.ingress.store.service.impl;

import az.ingress.store.config.security.JwtCredentials;
import az.ingress.store.config.security.JwtService;
import az.ingress.store.enums.dto.LoginResponseDto;
import az.ingress.store.enums.dto.RegistrationRequestDto;
import az.ingress.store.model.User;
import az.ingress.store.model.UserRole;
import az.ingress.store.repository.UserRepository;
import az.ingress.store.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private static final String USER_NOT_FOUND_MSG = "user with this email %s not found";

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;

    @Override
    public void register(RegistrationRequestDto request) {
        var user = userRepository.findByEmail(request.getEmail());
        if (user.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        userRepository.save(User.builder()
                .userRole(UserRole.USER)
                .email(request.getEmail())
                .enable(true)
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .locked(false)
                .password(bCryptPasswordEncoder.encode(request.getPassword()))
                .build());
    }

    @Override
    public LoginResponseDto login(RegistrationRequestDto request) {
        var userDetails = (User) loadUserByUsername(request.getEmail());
        boolean matches = bCryptPasswordEncoder.matches(request.getPassword(), userDetails.getPassword());
        if (!matches) {
            throw new RuntimeException("Email or password is invalid");
        }
        return LoginResponseDto.builder()
                .jwt(jwtService.issueToken(JwtCredentials.builder()
                        .id(userDetails.getId())
                        .role(userDetails.getUserRole())
                        .email(userDetails.getEmail())
                        .build()))
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(
                String.format(USER_NOT_FOUND_MSG, email)));
    }
}
