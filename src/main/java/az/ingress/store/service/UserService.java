package az.ingress.store.service;

import az.ingress.store.enums.dto.LoginResponseDto;
import az.ingress.store.enums.dto.RegistrationRequestDto;

public interface UserService {
    void register(RegistrationRequestDto request);

    LoginResponseDto login(RegistrationRequestDto request);

}
