package az.ingress.store.mapper;

import az.ingress.store.enums.dto.ProductDto;
import az.ingress.store.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {
    ProductDto mapEntityToDto(Product product);

    Product mapDtoToEntity(ProductDto productDto);
}
