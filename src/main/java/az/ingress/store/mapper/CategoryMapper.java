package az.ingress.store.mapper;

import az.ingress.store.enums.dto.CategoryDto;
import az.ingress.store.model.Category;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryMapper {
    CategoryDto mapEntityToDto(Category category);

    Category mapDtoToEntity(CategoryDto categoryDto);
}
