package az.ingress.store.model;

public enum UserRole {
    USER,
    ADMIN
}
