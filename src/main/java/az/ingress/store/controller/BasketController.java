package az.ingress.store.controller;

import az.ingress.store.enums.dto.BasketProductDto;
import az.ingress.store.service.BasketService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BasketController {
    private final BasketService basketService;

    @PutMapping("/basket")
    public ResponseEntity<Boolean> updateBasket(@Valid @RequestBody BasketProductDto productDto) {
        return new ResponseEntity<>(basketService.updateAndCreateBasket(productDto), HttpStatus.OK);
    }

}
