package az.ingress.store.controller;

import az.ingress.store.enums.dto.LoginResponseDto;
import az.ingress.store.enums.dto.RegistrationRequestDto;
import az.ingress.store.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/user")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/registration")
    public void register(@RequestBody RegistrationRequestDto request) {
        userService.register(request);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody RegistrationRequestDto request) {
       return ResponseEntity.ok(userService.login(request));
    }

}
