package az.ingress.store.controller;

import az.ingress.store.enums.dto.CategoryDto;
import az.ingress.store.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService service;

    @PostMapping
    public CategoryDto create(@Valid @RequestBody CategoryDto categoryDto) {
        return service.create(categoryDto);
    }

    @GetMapping("category/list")
    public List<CategoryDto> getAllCategory() {
        return service.getAllCategory();
    }
}

