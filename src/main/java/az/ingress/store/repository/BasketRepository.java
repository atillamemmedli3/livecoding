package az.ingress.store.repository;

import az.ingress.store.model.Basket;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BasketRepository extends JpaRepository<Basket, Long> {

    //it will be replaced with findByUserActiveBasket
    @Query("select b from Basket b")
    Optional<Basket> findOneBasket();

}
